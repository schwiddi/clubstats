-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema clubstats
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `clubstats` ;

-- -----------------------------------------------------
-- Schema clubstats
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `clubstats` DEFAULT CHARACTER SET utf8mb4 ;
USE `clubstats` ;

-- -----------------------------------------------------
-- Table `clubstats`.`clubs`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clubstats`.`clubs` ;

CREATE TABLE IF NOT EXISTS `clubstats`.`clubs` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(191) NOT NULL,
  `description` TEXT(1000) NULL DEFAULT NULL,
  `inserted` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` DATETIME NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `clubstats`.`disciplines`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clubstats`.`disciplines` ;

CREATE TABLE IF NOT EXISTS `clubstats`.`disciplines` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(191) NOT NULL,
  `description` TEXT(1000) NULL DEFAULT NULL,
  `club` INT(11) NOT NULL,
  `inserted` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` DATETIME NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_disciplines_clubs_idx` (`club` ASC),
  CONSTRAINT `fk_disciplines_clubs`
    FOREIGN KEY (`club`)
    REFERENCES `clubstats`.`clubs` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `clubstats`.`games`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clubstats`.`games` ;

CREATE TABLE IF NOT EXISTS `clubstats`.`games` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `discipline` INT(11) NOT NULL,
  `club` INT(11) NOT NULL,
  `inserted` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` DATETIME NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_games_disciplines_idx` (`discipline` ASC),
  INDEX `fk_games_clubs_idx` (`club` ASC),
  CONSTRAINT `fk_games_clubs`
    FOREIGN KEY (`club`)
    REFERENCES `clubstats`.`clubs` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_games_disciplines`
    FOREIGN KEY (`discipline`)
    REFERENCES `clubstats`.`disciplines` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `clubstats`.`membership_roles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clubstats`.`membership_roles` ;

CREATE TABLE IF NOT EXISTS `clubstats`.`membership_roles` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `clubstats`.`player_states`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clubstats`.`player_states` ;

CREATE TABLE IF NOT EXISTS `clubstats`.`player_states` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `clubstats`.`player_roles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clubstats`.`player_roles` ;

CREATE TABLE IF NOT EXISTS `clubstats`.`player_roles` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `clubstats`.`players`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clubstats`.`players` ;

CREATE TABLE IF NOT EXISTS `clubstats`.`players` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(191) NOT NULL,
  `password` CHAR(65) NULL DEFAULT NULL COMMENT 'go to CHAR(65) cause of this:\nhttps://stackoverflow.com/questions/247304/what-data-type-to-use-for-hashed-password-field-and-what-length',
  `email` VARCHAR(191) NULL DEFAULT NULL,
  `state` INT(11) NOT NULL,
  `role` INT(11) NOT NULL,
  `registration_key` TEXT(1000) NULL DEFAULT NULL,
  `inserted` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` DATETIME NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC),
  INDEX `fk_players_player_states_idx` (`state` ASC),
  INDEX `fk_players_player_roles_idx` (`role` ASC),
  CONSTRAINT `fk_players_player_states`
    FOREIGN KEY (`state`)
    REFERENCES `clubstats`.`player_states` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_players_player_roles`
    FOREIGN KEY (`role`)
    REFERENCES `clubstats`.`player_roles` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `clubstats`.`memberships`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clubstats`.`memberships` ;

CREATE TABLE IF NOT EXISTS `clubstats`.`memberships` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `player` INT(11) NOT NULL,
  `club` INT(11) NOT NULL,
  `role` INT(11) NOT NULL,
  `inserted` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` DATETIME NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_memberships_players_idx` (`player` ASC),
  INDEX `fk_memberships_clubs_idx` (`club` ASC),
  INDEX `fk_memberships_roles_idx` (`role` ASC),
  UNIQUE INDEX `player_club_UNIQUE` (`player` ASC, `club` ASC),
  CONSTRAINT `fk_memberships_clubs`
    FOREIGN KEY (`club`)
    REFERENCES `clubstats`.`clubs` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_memberships_roles`
    FOREIGN KEY (`role`)
    REFERENCES `clubstats`.`membership_roles` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_memberships_players`
    FOREIGN KEY (`player`)
    REFERENCES `clubstats`.`players` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `clubstats`.`participations`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clubstats`.`participations` ;

CREATE TABLE IF NOT EXISTS `clubstats`.`participations` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `game` INT(11) NOT NULL,
  `player` INT(11) NOT NULL,
  `winner` TINYINT(1) NOT NULL,
  `inserted` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` DATETIME NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_participations_games_idx` (`game` ASC),
  INDEX `fk_participations_players_idx` (`player` ASC),
  CONSTRAINT `fk_participations_games`
    FOREIGN KEY (`game`)
    REFERENCES `clubstats`.`games` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_participations_players`
    FOREIGN KEY (`player`)
    REFERENCES `clubstats`.`players` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `clubstats`.`mailing_types`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clubstats`.`mailing_types` ;

CREATE TABLE IF NOT EXISTS `clubstats`.`mailing_types` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(191) NOT NULL,
  `description` TEXT(1000) NULL DEFAULT NULL,
  `inserted` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` DATETIME NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `clubstats`.`mailings`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clubstats`.`mailings` ;

CREATE TABLE IF NOT EXISTS `clubstats`.`mailings` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `player` INT(11) NOT NULL,
  `type` INT(11) NOT NULL,
  `state` INT(11) NOT NULL DEFAULT '0',
  `inserted` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` DATETIME NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_mailings_players_idx` (`player` ASC),
  INDEX `fk_mailings_mailing_type_idx` (`type` ASC),
  CONSTRAINT `fk_mailings_players`
    FOREIGN KEY (`player`)
    REFERENCES `clubstats`.`players` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_mailings_mailing_type`
    FOREIGN KEY (`type`)
    REFERENCES `clubstats`.`mailing_types` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `clubstats`.`log_api_requests`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clubstats`.`log_api_requests` ;

CREATE TABLE IF NOT EXISTS `clubstats`.`log_api_requests` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `request` TEXT(1000) NOT NULL,
  `inserted` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

USE `clubstats` ;

-- -----------------------------------------------------
-- procedure register_new_player
-- -----------------------------------------------------

USE `clubstats`;
DROP procedure IF EXISTS `clubstats`.`register_new_player`;

DELIMITER $$
USE `clubstats`$$
CREATE PROCEDURE `register_new_player`(
	IN name VARCHAR(255),
    IN password VARCHAR(255),
    IN email VARCHAR(255)
)
BEGIN
  DECLARE exit handler for sqlexception
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  DECLARE exit handler for sqlwarning
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  START TRANSACTION;
	INSERT INTO players (name, password, email, state, role) VALUES (name, password, email, '3', '2');
	IF LAST_INSERT_ID() != '0' THEN
    	SELECT id, inserted FROM players WHERE id = LAST_INSERT_ID();
    END IF;
    COMMIT;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure delete_all_data
-- -----------------------------------------------------

USE `clubstats`;
DROP procedure IF EXISTS `clubstats`.`delete_all_data`;

DELIMITER $$
USE `clubstats`$$
CREATE PROCEDURE `delete_all_data`()
BEGIN
  DECLARE exit handler for sqlexception
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  DECLARE exit handler for sqlwarning
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  START TRANSACTION;
	DELETE FROM participations;
    DELETE FROM games;
    DELETE FROM disciplines;
    DELETE FROM memberships;
    DELETE FROM clubs;
    DELETE FROM players;
    DELETE FROM log_api_requests;
    COMMIT;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure check_open_registration
-- -----------------------------------------------------

USE `clubstats`;
DROP procedure IF EXISTS `clubstats`.`check_open_registration`;

DELIMITER $$
USE `clubstats`$$
CREATE PROCEDURE `check_open_registration`(
    IN p_hash TEXT(255)
)
BEGIN
  DECLARE exit handler for sqlexception
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  DECLARE exit handler for sqlwarning
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  START TRANSACTION;
    SELECT id, inserted FROM players WHERE state = '3' AND registration_key = p_hash;
    COMMIT;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure complete_registration
-- -----------------------------------------------------

USE `clubstats`;
DROP procedure IF EXISTS `clubstats`.`complete_registration`;

DELIMITER $$
USE `clubstats`$$
CREATE PROCEDURE `complete_registration`(
    IN p_hash TEXT(255)
)
BEGIN
  DECLARE exit handler for sqlexception
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  DECLARE exit handler for sqlwarning
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  START TRANSACTION;
    UPDATE players SET state = '4' WHERE state = '3' AND registration_key = p_hash;
    COMMIT;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure create_club
-- -----------------------------------------------------

USE `clubstats`;
DROP procedure IF EXISTS `clubstats`.`create_club`;

DELIMITER $$
USE `clubstats`$$
CREATE PROCEDURE `create_club`(
	IN p_name VARCHAR(255),
    IN p_description VARCHAR(1000),
    IN p_playerid INT(11)
)
BEGIN
  DECLARE exit handler for sqlexception
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  DECLARE exit handler for sqlwarning
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;
  
  START TRANSACTION;
    INSERT INTO clubs (name, description) VALUES (p_name,  p_description);
    SET @y = LAST_INSERT_ID();
    INSERT INTO memberships (player, club, role) VALUES (p_playerid, LAST_INSERT_ID(), '1');
    SELECT id, name, description FROM clubs where id = @y;
	COMMIT;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure get_player_by_mail
-- -----------------------------------------------------

USE `clubstats`;
DROP procedure IF EXISTS `clubstats`.`get_player_by_mail`;

DELIMITER $$
USE `clubstats`$$
CREATE PROCEDURE `get_player_by_mail`(
    IN p_email VARCHAR(255)
)
BEGIN
  DECLARE exit handler for sqlexception
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  DECLARE exit handler for sqlwarning
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  START TRANSACTION;
    SELECT * FROM players
    WHERE email = p_email;
    COMMIT;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure get_memberships_by_player_id
-- -----------------------------------------------------

USE `clubstats`;
DROP procedure IF EXISTS `clubstats`.`get_memberships_by_player_id`;

DELIMITER $$
USE `clubstats`$$
CREATE PROCEDURE `get_memberships_by_player_id`(
	IN p_player_id INT(11)
)
BEGIN
  DECLARE exit handler for sqlexception
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  DECLARE exit handler for sqlwarning
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  START TRANSACTION;
    SELECT
	  m.id AS id,
      m.club AS club_id,
      c.name AS club_name,
      c.description AS club_description,
      m.role AS role_id,
      mr.name AS role_name
    FROM memberships m
    JOIN (clubs c, membership_roles mr)
	  ON (m.club = c.id AND m.role = mr.id)
    WHERE m.player = p_player_id;
    COMMIT;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure get_player_with_roles_and_state_by_id
-- -----------------------------------------------------

USE `clubstats`;
DROP procedure IF EXISTS `clubstats`.`get_player_with_roles_and_state_by_id`;

DELIMITER $$
USE `clubstats`$$
CREATE PROCEDURE `get_player_with_roles_and_state_by_id`(
	IN p_player_id INT(11)
)
BEGIN
  DECLARE exit handler for sqlexception
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  DECLARE exit handler for sqlwarning
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  START TRANSACTION;
    SELECT
      p.id AS id,
	  p.name AS name,
      p.email AS email,
      p.inserted AS inserted,
      p.updated AS updated,
      p.role AS role_id,
      pr.name AS role_name,
      p.state AS state_id,
      ps.name AS state_name
    FROM players p
    JOIN (player_states ps, player_roles pr)
	  ON (p.state = ps.id AND p.role = pr.id)
    WHERE p.id = p_player_id;
    COMMIT;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure createDiscipline
-- -----------------------------------------------------

USE `clubstats`;
DROP procedure IF EXISTS `clubstats`.`createDiscipline`;

DELIMITER $$
USE `clubstats`$$
CREATE PROCEDURE `createDiscipline`(
	IN pname VARCHAR(191),
    IN pdescription TEXT(1000),
    IN pclubid INT(11)
)
BEGIN
  DECLARE exit handler for sqlexception
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  DECLARE exit handler for sqlwarning
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;
  
  START TRANSACTION;
	INSERT INTO disciplines (name, description, club) VALUES (pname,  pdescription, pclubid);
    SELECT * FROM disciplines WHERE id = LAST_INSERT_ID();
    COMMIT;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure get_player_state_by_player_id
-- -----------------------------------------------------

USE `clubstats`;
DROP procedure IF EXISTS `clubstats`.`get_player_state_by_player_id`;

DELIMITER $$
USE `clubstats`$$
CREATE PROCEDURE `get_player_state_by_player_id`(
    IN p_player_id INT(11)
)
BEGIN
  DECLARE exit handler for sqlexception
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  DECLARE exit handler for sqlwarning
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  START TRANSACTION;
    SELECT state AS state FROM players
    WHERE id = p_player_id;
    COMMIT;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure create_player
-- -----------------------------------------------------

USE `clubstats`;
DROP procedure IF EXISTS `clubstats`.`create_player`;

DELIMITER $$
USE `clubstats`$$
CREATE PROCEDURE `create_player`(
	IN p_name VARCHAR(255),
    IN p_club_id INT (11)
)
BEGIN
  DECLARE exit handler for sqlexception
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  DECLARE exit handler for sqlwarning
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  START TRANSACTION;
    INSERT INTO players (name, state, role) VALUES (p_name, '1', '2');
    SET @player_id = LAST_INSERT_ID();
    INSERT INTO memberships (player, club, role) VALUES (@player_id, p_club_id, '3');
	SELECT id, name FROM players WHERE id = @player_id;
    COMMIT;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure create_game
-- -----------------------------------------------------

USE `clubstats`;
DROP procedure IF EXISTS `clubstats`.`create_game`;

DELIMITER $$
USE `clubstats`$$
CREATE PROCEDURE `create_game`(
	IN p_discipline INT(11),
    IN p_club INT(11),
    IN p_winner INT(11),
    IN p_looser INT(11)
)
BEGIN
  DECLARE exit handler for sqlexception
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  DECLARE exit handler for sqlwarning
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;
  
  START TRANSACTION;
	INSERT INTO games (discipline, club) VALUES (p_discipline, p_club);
    SET @v_gameid = LAST_INSERT_ID();
    
    -- do the insert for the winner
    INSERT INTO participations(game, player, winner) VALUES (@v_gameid, p_winner, 1);
    INSERT INTO participations(game, player, winner) VALUES (@v_gameid, p_looser, 0);
    SELECT id, inserted FROM games WHERE id = @v_gameid;
  COMMIT;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure write_register_hash
-- -----------------------------------------------------

USE `clubstats`;
DROP procedure IF EXISTS `clubstats`.`write_register_hash`;

DELIMITER $$
USE `clubstats`$$
CREATE PROCEDURE `write_register_hash`(
	IN p_id INT(11),
	IN p_hash TEXT(255)
)
BEGIN
  DECLARE exit handler for sqlexception
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  DECLARE exit handler for sqlwarning
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  START TRANSACTION;
    UPDATE players SET registration_key = p_hash WHERE id = p_id;
    COMMIT;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure updateDiscipline
-- -----------------------------------------------------

USE `clubstats`;
DROP procedure IF EXISTS `clubstats`.`updateDiscipline`;

DELIMITER $$
USE `clubstats`$$
CREATE PROCEDURE `updateDiscipline`(
	IN p_id INT(11),
    IN p_name VARCHAR(191),
    IN p_description TEXT(1000)
)
BEGIN
  DECLARE exit handler for sqlexception
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  DECLARE exit handler for sqlwarning
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;
  
  START TRANSACTION;
	UPDATE disciplines SET name = p_name, description = p_description WHERE (id = p_id);
    SELECT * FROM disciplines WHERE id = p_id;
    COMMIT;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure createDisciplineNoDesc
-- -----------------------------------------------------

USE `clubstats`;
DROP procedure IF EXISTS `clubstats`.`createDisciplineNoDesc`;

DELIMITER $$
USE `clubstats`$$
CREATE PROCEDURE `createDisciplineNoDesc`(
	IN pname VARCHAR(191),
    IN pclubid INT(11)
)
BEGIN
  DECLARE exit handler for sqlexception
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  DECLARE exit handler for sqlwarning
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;
  
  START TRANSACTION;
	INSERT INTO disciplines (name, club) VALUES (pname, pclubid);
    SELECT * FROM disciplines WHERE id = LAST_INSERT_ID();
    COMMIT;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure updateDisciplineNoDesc
-- -----------------------------------------------------

USE `clubstats`;
DROP procedure IF EXISTS `clubstats`.`updateDisciplineNoDesc`;

DELIMITER $$
USE `clubstats`$$
CREATE PROCEDURE `updateDisciplineNoDesc`(
	IN p_id INT(11),
    IN p_name VARCHAR(191)
)
BEGIN
  DECLARE exit handler for sqlexception
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  DECLARE exit handler for sqlwarning
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;
  
  START TRANSACTION;
	UPDATE disciplines SET name = p_name WHERE (id = p_id);
    SELECT * FROM disciplines WHERE id = p_id;
    COMMIT;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure getMemRole
-- -----------------------------------------------------

USE `clubstats`;
DROP procedure IF EXISTS `clubstats`.`getMemRole`;

DELIMITER $$
USE `clubstats`$$
CREATE PROCEDURE `getMemRole`(
	IN p_player INT(11),
    IN p_club INT(11)
)
BEGIN
  DECLARE exit handler for sqlexception
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  DECLARE exit handler for sqlwarning
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;
  
  START TRANSACTION;
	SELECT m.role
	FROM	players p
    JOIN memberships m
		ON p.id = m.player
	WHERE p.id = p_player AND m.club = p_club;
    COMMIT;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure deleteDiscipline
-- -----------------------------------------------------

USE `clubstats`;
DROP procedure IF EXISTS `clubstats`.`deleteDiscipline`;

DELIMITER $$
USE `clubstats`$$
CREATE PROCEDURE `deleteDiscipline`(
	IN p_id INT(11)
)
BEGIN
  DECLARE exit handler for sqlexception
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  DECLARE exit handler for sqlwarning
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;
  
  START TRANSACTION;
	DELETE FROM disciplines WHERE id = p_id;
    COMMIT;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure getDisciplinesFromClub
-- -----------------------------------------------------

USE `clubstats`;
DROP procedure IF EXISTS `clubstats`.`getDisciplinesFromClub`;

DELIMITER $$
USE `clubstats`$$
CREATE PROCEDURE `getDisciplinesFromClub`(
    IN p_club INT(11)
)
BEGIN
  DECLARE exit handler for sqlexception
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  DECLARE exit handler for sqlwarning
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;
  
  START TRANSACTION;
	SELECT * FROM disciplines WHERE club = p_club;
    COMMIT;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure deleteGame
-- -----------------------------------------------------

USE `clubstats`;
DROP procedure IF EXISTS `clubstats`.`deleteGame`;

DELIMITER $$
USE `clubstats`$$
CREATE PROCEDURE `deleteGame`(
	IN p_id INT(11)
)
BEGIN
  DECLARE exit handler for sqlexception
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  DECLARE exit handler for sqlwarning
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;
  
  START TRANSACTION;
	DELETE FROM games WHERE id = p_id;
    COMMIT;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure invitePlayer
-- -----------------------------------------------------

USE `clubstats`;
DROP procedure IF EXISTS `clubstats`.`invitePlayer`;

DELIMITER $$
USE `clubstats`$$
CREATE PROCEDURE `invitePlayer`(
	IN p_id INT(11),
    IN p_email VARCHAR(191)
)
BEGIN
  DECLARE exit handler for sqlexception
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  DECLARE exit handler for sqlwarning
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;
  
  START TRANSACTION;
	UPDATE players SET email = p_email, state = '2' WHERE id = p_id;
    SELECT id, name, email, state, role, inserted, updated FROM players WHERE id = p_id;
    COMMIT;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure createMembership
-- -----------------------------------------------------

USE `clubstats`;
DROP procedure IF EXISTS `clubstats`.`createMembership`;

DELIMITER $$
USE `clubstats`$$
CREATE PROCEDURE `createMembership`(
	IN p_player INT(11),
    IN p_club INT(11),
    IN p_role INT(11)
)
BEGIN
  DECLARE exit handler for sqlexception
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;

  DECLARE exit handler for sqlwarning
  BEGIN
    ROLLBACK;
    RESIGNAL;
  END;
  
  START TRANSACTION;
	INSERT INTO memberships (player, club, role) VALUES (p_player, p_club, p_role);
  COMMIT;
END$$

DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `clubstats`.`membership_roles`
-- -----------------------------------------------------
START TRANSACTION;
USE `clubstats`;
INSERT INTO `clubstats`.`membership_roles` (`id`, `name`) VALUES (DEFAULT, 'founder');
INSERT INTO `clubstats`.`membership_roles` (`id`, `name`) VALUES (DEFAULT, 'logger');
INSERT INTO `clubstats`.`membership_roles` (`id`, `name`) VALUES (DEFAULT, 'active');
INSERT INTO `clubstats`.`membership_roles` (`id`, `name`) VALUES (DEFAULT, 'inactive');

COMMIT;


-- -----------------------------------------------------
-- Data for table `clubstats`.`player_states`
-- -----------------------------------------------------
START TRANSACTION;
USE `clubstats`;
INSERT INTO `clubstats`.`player_states` (`id`, `name`) VALUES (DEFAULT, 'player');
INSERT INTO `clubstats`.`player_states` (`id`, `name`) VALUES (DEFAULT, 'invited');
INSERT INTO `clubstats`.`player_states` (`id`, `name`) VALUES (DEFAULT, 'registered');
INSERT INTO `clubstats`.`player_states` (`id`, `name`) VALUES (DEFAULT, 'active');
INSERT INTO `clubstats`.`player_states` (`id`, `name`) VALUES (DEFAULT, 'inactive');

COMMIT;


-- -----------------------------------------------------
-- Data for table `clubstats`.`player_roles`
-- -----------------------------------------------------
START TRANSACTION;
USE `clubstats`;
INSERT INTO `clubstats`.`player_roles` (`id`, `name`) VALUES (DEFAULT, 'admin');
INSERT INTO `clubstats`.`player_roles` (`id`, `name`) VALUES (DEFAULT, 'standard');

COMMIT;


-- -----------------------------------------------------
-- Data for table `clubstats`.`mailing_types`
-- -----------------------------------------------------
START TRANSACTION;
USE `clubstats`;
INSERT INTO `clubstats`.`mailing_types` (`id`, `name`, `description`, `inserted`, `updated`) VALUES (DEFAULT, 'completeRegistration', NULL, NULL, NULL);

COMMIT;

-- begin attached script 'script'
-- Add 10 players
INSERT INTO `clubstats`.`players` (`name`, `password`, `email`, `state`, `role`, `registration_key`) VALUES ('player1', 'bla', 'player1@mail.com', '4', '2', '123');
INSERT INTO `clubstats`.`players` (`name`, `password`, `email`, `state`, `role`, `registration_key`) VALUES ('player2', 'bla', 'player2@mail.com', '4', '2', '123');
INSERT INTO `clubstats`.`players` (`name`, `password`, `email`, `state`, `role`, `registration_key`) VALUES ('player3', 'bla', 'player3@mail.com', '4', '2', '123');
INSERT INTO `clubstats`.`players` (`name`, `password`, `email`, `state`, `role`, `registration_key`) VALUES ('player4', 'bla', 'player4@mail.com', '4', '2', '123');
INSERT INTO `clubstats`.`players` (`name`, `password`, `email`, `state`, `role`, `registration_key`) VALUES ('player5', 'bla', 'player5@mail.com', '4', '2', '123');
INSERT INTO `clubstats`.`players` (`name`, `password`, `email`, `state`, `role`, `registration_key`) VALUES ('player6', 'bla', 'player6@mail.com', '3', '2', '123');
INSERT INTO `clubstats`.`players` (`name`, `password`, `email`, `state`, `role`, `registration_key`) VALUES ('player7', 'bla', 'player7@mail.com', '3', '2', '123');
INSERT INTO `clubstats`.`players` (`name`, `password`, `email`, `state`, `role`, `registration_key`) VALUES ('player8', 'bla', 'player8@mail.com', '1', '2', '123');
INSERT INTO `clubstats`.`players` (`name`, `password`, `email`, `state`, `role`, `registration_key`) VALUES ('player9', 'bla', 'player9@mail.com', '1', '2', '123');
INSERT INTO `clubstats`.`players` (`name`, `password`, `email`, `state`, `role`, `registration_key`) VALUES ('player10', 'bla', 'player10@mail.com', '1', '2', '123');

-- add two clubs
INSERT INTO `clubstats`.`clubs` (`name`) VALUES ('club1');
INSERT INTO `clubstats`.`clubs` (`name`) VALUES ('club2');

-- now add 1-5 to club 1
INSERT INTO `clubstats`.`memberships` (`player`, `club`, `role`) VALUES ('1', '1', '1');
INSERT INTO `clubstats`.`memberships` (`player`, `club`, `role`) VALUES ('2', '1', '2');
INSERT INTO `clubstats`.`memberships` (`player`, `club`, `role`) VALUES ('3', '1', '3');
INSERT INTO `clubstats`.`memberships` (`player`, `club`, `role`) VALUES ('4', '1', '3');
INSERT INTO `clubstats`.`memberships` (`player`, `club`, `role`) VALUES ('5', '1', '3');

-- then the other five to the second club
INSERT INTO `clubstats`.`memberships` (`player`, `club`, `role`) VALUES ('6', '2', '1');
INSERT INTO `clubstats`.`memberships` (`player`, `club`, `role`) VALUES ('7', '2', '1');
INSERT INTO `clubstats`.`memberships` (`player`, `club`, `role`) VALUES ('8', '2', '2');
INSERT INTO `clubstats`.`memberships` (`player`, `club`, `role`) VALUES ('9', '2', '3');
INSERT INTO `clubstats`.`memberships` (`player`, `club`, `role`) VALUES ('10', '2', '3');

-- add pool as the base dicipline for club 1
INSERT INTO `clubstats`.`disciplines` (`name`, `club`) VALUES ('Pool', '1');

-- in club two there are to discis
INSERT INTO `clubstats`.`disciplines` (`name`, `club`) VALUES ('Dart', '2');
INSERT INTO `clubstats`.`disciplines` (`name`, `club`) VALUES ('Bowling', '2');


-- then add three games to every club
-- game 1 club 1
INSERT INTO `clubstats`.`games` (`discipline`, `club`) VALUES ('1', '1');
INSERT INTO `clubstats`.`participations` (`game`, `player`, `winner`) VALUES ('1', '1', '1');
INSERT INTO `clubstats`.`participations` (`game`, `player`, `winner`) VALUES ('1', '2', '0');
-- game 2 club 1
INSERT INTO `clubstats`.`games` (`discipline`, `club`) VALUES ('1', '1');
INSERT INTO `clubstats`.`participations` (`game`, `player`, `winner`) VALUES ('2', '1', '1');
INSERT INTO `clubstats`.`participations` (`game`, `player`, `winner`) VALUES ('2', '3', '0');
-- game 3 club 1
INSERT INTO `clubstats`.`games` (`discipline`, `club`) VALUES ('1', '1');
INSERT INTO `clubstats`.`participations` (`game`, `player`, `winner`) VALUES ('3', '1', '0');
INSERT INTO `clubstats`.`participations` (`game`, `player`, `winner`) VALUES ('3', '4', '1');



-- game 1 club 2
INSERT INTO `clubstats`.`games` (`discipline`, `club`) VALUES ('2', '2');
INSERT INTO `clubstats`.`participations` (`game`, `player`, `winner`) VALUES ('4', '6', '0');
INSERT INTO `clubstats`.`participations` (`game`, `player`, `winner`) VALUES ('4', '7', '1');
-- game 2 club 2
INSERT INTO `clubstats`.`games` (`discipline`, `club`) VALUES ('3', '2');
INSERT INTO `clubstats`.`participations` (`game`, `player`, `winner`) VALUES ('5', '9', '0');
INSERT INTO `clubstats`.`participations` (`game`, `player`, `winner`) VALUES ('5', '10', '1');
-- game 3 club 2
INSERT INTO `clubstats`.`games` (`discipline`, `club`) VALUES ('2', '2');
INSERT INTO `clubstats`.`participations` (`game`, `player`, `winner`) VALUES ('6', '7', '0');
INSERT INTO `clubstats`.`participations` (`game`, `player`, `winner`) VALUES ('6', '8', '1');
-- end attached script 'script'
