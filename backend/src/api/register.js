const express = require('express');
const register = express.Router();
const Joi = require('joi');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const jwthen = require('jwt-then');
const log = require('../common/logger');
const db = require('../db/db_connection');
const { registerPlayer, } = require('../schemas/joiInVal');

register.post('/', (req, res) => {
  Joi.validate(req.body, registerPlayer)
    .then(() => {
      bcrypt.hash(req.body.password, 10)
        .then(hashedpw => {
          db.query(`CALL register_new_player('${req.body.name}', '${hashedpw}', '${req.body.email}');`)
            .then(rows => {
              const newPlayer = rows[0][0][0];
              if (!newPlayer) {
                res.sendStatus(500);
              } else {
                res.status(201).send(newPlayer);
                const registerToken = jwt.sign(
                  {
                    id: newPlayer.id,
                  },
                  process.env.BACKEND_JWT_KEY,
                  {
                    expiresIn: process.env.BACKEND_JWT_EXPIRE,
                  }
                );
                db.query(`CALL write_register_hash('${newPlayer.id}', '${registerToken}');`)
                  .catch(err => {
                    log.error(`logger="writeRegisterHashToDB" module="db" err="${err.message}"`);
                  });
              }
            })
            .catch(err => {
              if (err.errno === 1062) {
                res.status(409).send(err);
              } else {
                res.status(500).send(err);
              }
              log.error(`db: ${err.message}`);
            });
        })
        .catch(err => {
          res.status(500).send(err);
          log.error(`bcrypt: ${err.message}`);
        });
    })
    .catch(err => {
      res.status(400).send(`${err.name}: ${err.details[0].message} `);
      log.warn(`Joi: ${err.message}`);
    });
});

register.get('/:registerToken', (req, res) => {
  jwthen.verify(req.params.registerToken, process.env.BACKEND_JWT_KEY)
    .then(payload => {
      db.query(`CALL check_open_registration('${req.params.registerToken}');`)
        .then(rows => {
          const player = rows[0][0][0];
          if (!player) {
            res.sendStatus(400);
            log.warn('unknown or allready used regKey!!!');
          } else {
            db.query(`CALL complete_registration('${req.params.registerToken}');`)
              .then(() => {
                res.sendStatus(200);
                log.info(`logger="completeRegister" module="db" msg="player activated" id="${payload.id}"`);
              }).catch(err => {
                res.sendStatus(500);
                log.error(err.message);
              });
          }
        })
        .catch(err => {
          res.sendStatus(500);
          log.error(err.message);
        });
    })
    .catch(err => {
      res.sendStatus(400);
      log.warn(`logger = "activatePlayer" module = "jwt" err = "${err.message}"`);
    });

});

module.exports = register;
