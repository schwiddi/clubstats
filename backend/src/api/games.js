const express = require('express');
const games = express.Router();
const Joi = require('joi');
const _ = require('lodash');
const db = require('../db/db_connection');
const log = require('../common/logger');
const { createGame, deleteGames, } = require('../schemas/games');
const addPlayerInfoToReq = require('../middleware/addPlayerInfoToReq');
const addMemRole = require('../middleware/addMemRole');

games.post('/', addPlayerInfoToReq, addMemRole, async (req, res) => {
  if (req.memRole === 1 || req.memRole === 2) {
    try {
      await Joi.validate(req.body, createGame);
    } catch (error) {
      return res.status(400).send(error);
    }

    try {
      let reducedParticipants = _.remove(
        req.body.participants, function (el) {
          return el !== req.body.winner;
        });
      let looser = reducedParticipants[0];
      const rows = await db.query(`CALL create_game(${req.body.discipline}, ${req.body.club}, ${req.body.winner}, ${looser});`);
      if (!rows[0][0][0]) {
        log.warn('logger="createGame" err="response from db is empty"');
        return res.sendStatus(500);
      } else {
        return res.status(201).send(rows[0][0][0]);
      }
    } catch (error) {
      log.error(`logger="createGame" module="db" err="${error.message}"`);
      return res.status(400).send(error);
    }
  } else {
    return res.sendStatus(403);
  }
});

games.delete('/', addPlayerInfoToReq, addMemRole, async (req, res) => {
  if (req.memRole === 1 || req.memRole === 2) {
    try {
      await Joi.validate(req.body, deleteGames);
    } catch (error) {
      return res.status(400).send(error);
    }

    try {
      req.body.games.forEach(element => {
        db.query(`CALL deleteGame(${element})`)
          .catch(err => {
            log.error(err.message);
          });
      });
      return res.sendStatus(200);
    } catch (error) {
      return log.error(error.message);
    }
  } else {
    return res.sendStatus(403);
  }
});

module.exports = games;
