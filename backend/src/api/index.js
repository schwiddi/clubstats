const express = require('express');
const admin = require('./admin');
const register = require('./register');
const auth = require('./auth');
const clubs = require('./clubs');
const disciplines = require('./disciplines');
const players = require('./players');
const games = require('./games');
const invites = require('./invites');
const memberships = require('./memberships');
const api = express.Router();

const defaultRes = {
  'response': 'Hello!!!!',
};

api.use('/admin/', admin);
api.use('/register/', register);
api.use('/auth/', auth);
api.use('/clubs/', clubs);
api.use('/disciplines/', disciplines);
api.use('/players/', players);
api.use('/games/', games);
api.use('/invites/', invites);
api.use('/memberships/', memberships);

api.get('/', (req, res) => {
  res.status(200).send(defaultRes);
});

module.exports = api;
