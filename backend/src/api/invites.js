const express = require('express');
const invites = express.Router();
const addPlayerInfoToReq = require('../middleware/addPlayerInfoToReq');
const Joi = require('joi');
const { invitePlayer, } = require('../schemas/invites');
const log = require('../common/logger');
const db = require('../db/db_connection');
const jwt = require('jsonwebtoken');

invites.put('/', addPlayerInfoToReq, async (req, res) => {
  try {
    await Joi.validate(req.body, invitePlayer);
  } catch (error) {
    log.warn(error.message);
    return res.status(400).send(error);
  }

  try {
    const rows = await db.query(`CALL invitePlayer(${req.body.player}, '${req.body.email}');`);
    const invitedPlayer = rows[0][0][0];
    if (!invitedPlayer) {
      return res.sendStatus(400);
    } else {
      res.status(200).send(invitedPlayer);
      const registerToken = jwt.sign(
        {
          id: invitedPlayer.id,
        },
        process.env.BACKEND_JWT_KEY,
        {
          expiresIn: process.env.BACKEND_JWT_EXPIRE,
        }
      );
      await db.query(`CALL write_register_hash('${invitedPlayer.id}', '${registerToken}');`);
      return;
    }
  } catch (error) {
    log.error(error.message);
    return res.status(500).send(error);
  }
});

module.exports = invites;
