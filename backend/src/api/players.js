const express = require('express');
const players = express.Router();
const Joi = require('joi');
const addPlayerInfoToReq = require('../middleware/addPlayerInfoToReq');
const db = require('../db/db_connection');
const log = require('../common/logger');
const { createSimplePlayer, } = require('../schemas/joiInVal');
const addMemRole = require('../middleware/addMemRole');

players.get('/me', addPlayerInfoToReq, (req, res) => {
  let meObject = {};
  db.query(`CALL get_player_with_roles_and_state_by_id('${req.player.id}');`)
    .then((rows) => {
      let dbres = rows[0][0][0];
      meObject = {
        player_id: dbres.player_id,
        name: dbres.name,
        email: dbres.email,
        inserted: dbres.inserted,
        updated: dbres.updated,
        role: {
          id: dbres.role_id,
          name: dbres.role_text,
        },
        state: {
          id: dbres.state_id,
          name: dbres.state_text,
        },
      };
      return db.query(`CALL get_memberships_by_player_id('${req.player.id}');`);
    })
    .then((rows) => {
      let dbres = rows[0][0];
      let tmpArr = [];
      dbres.forEach(element => {
        tmpArr.push(element);
      });
      meObject.memberships = tmpArr;
      res.status(200).send(meObject);
      log.info(`logger="meAPI" msg="successfull call me api" player="${req.player.id}"`);
    })
    .catch(err => {
      res.status(500).send(err.message);
      log.warn(`logger="meAPI" err="${err.message}" player="${req.player.id}"`);
    });
});

players.post('/', addPlayerInfoToReq, addMemRole, (req, res) => {
  if (req.memRole === 1 || req.memRole === 2) {
    Joi.validate(req.body, createSimplePlayer)
      .then(() => {
        db.query(`CALL create_player('${req.body.name}','${req.body.club}')`)
          .then(rows => {
            const player = rows[0][0][0];
            res.status(201).send(player);
            log.info(`logger="postPlayer" msg="new player ${player.name} created for club ${req.body.club}"`);
          })
          .catch(err => {
            res.status(400).send(err);
            log.warn(`logger="postPlayer" err="${err.message}"`);
          });
      })
      .catch(err => {
        res.status(400).send(err);
        log.warn(`logger="postPlayer" err="${err.message}"`);
      });
  } else {
    res.sendStatus(403);
  }
});

module.exports = players;
