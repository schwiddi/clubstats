const express = require('express');
const log = require('../common/logger');
const db = require('../db/db_connection');
const admin = express.Router();

admin.get('/clearall', async (req, res) => {
  if (process.env.NODE_ENV === 'dev') {
    try {
      await db.query('CALL delete_all_data();');
      log.warn('cleared all tables...');
      return res.sendStatus(200);
    } catch (error) {
      log.error(error.message);
      return res.sendStatus(500);
    }
  } else {
    log.warn('DB truncate only on dev allowed!!!');
    return res.sendStatus(400);
  }
});

admin.post('/setplayeractive/:player_id', async (req, res) => {
  if (process.env.NODE_ENV === 'dev') {
    try {
      await db.query(`UPDATE players SET state = '4' WHERE id = '${req.params.player_id}';`);
      return res.sendStatus(200);
    } catch (error) {
      log.error(error.message);
      return res.sendStatus(500);
    }
  } else {
    return res.sendStatus(400);
  }
});

module.exports = admin;
