const express = require('express');
const Joi = require('joi');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const db = require('../db/db_connection');
const log = require('../common/logger');
const { do_auth, } = require('../schemas/joiInVal');
const auth = express.Router();

auth.post('/', async (req, res) => {
  try {
    await Joi.validate(req.body, do_auth);
  } catch (error) {
    log.warn(error.message);
    return res.sendStatus(400); // on login we dont say the caller why he messed up with us only that the goven shit was not correct
  }

  try {
    const rows = await db.query(`CALL get_player_by_mail('${req.body.email}');`);
    if (!rows[0][0][0]) {
      log.warn(`auth with unknown mail! ${req.body.email}`);
      return res.sendStatus(400); // when a unknown mail is given we dont let the caller know it
    }

    if (rows[0][0][0].state != 4) {
      log.warn(`auth for player that is not active! ${req.body.email}`);
      return res.status(401).send('player not active');
    }

    try {
      const compRes = await bcrypt.compare(req.body.password, rows[0][0][0].password); // bcrypt reoslves with true or false
      if (compRes) {
        const jwtToken = jwt.sign(
          {
            player_id: rows[0][0][0].id,
          },
          process.env.BACKEND_JWT_KEY,
          {
            expiresIn: process.env.BACKEND_JWT_EXPIRE,
          }
        );
        log.info(`player ${req.body.email} successfull auth and new token generated ${jwtToken}`);
        return res
          .status(200)
          .header('x-auth-token', jwtToken)
          .header('access-control-expose-headers', 'x-auth-token')
          .send('OK');
      } else {
        log.warn(`auth with wrong password!! ${req.body.email} and pw: ${req.body.password}`);
        return res.sendStatus(400);
      }
    } catch (error) {
      log.error(error.message);
      return res.status(500).send(error); // if we have a problem while comparing we let the caller know
    }
  } catch (error) {
    log.error(error.message);
    return res.status(500).send(error); // when we have a db problem we let the client know that it was our problem in the backend
  }
});

module.exports = auth;