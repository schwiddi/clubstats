const express = require('express');
const clubs = express.Router();
const Joi = require('joi');
const db = require('../db/db_connection');
const log = require('../common/logger');
const { create_club, } = require('../schemas/joiInVal');
const addPlayerInfoToReq = require('../middleware/addPlayerInfoToReq');

clubs.post('/', addPlayerInfoToReq, async (req, res) => {
  try {
    await Joi.validate(req.body, create_club);
  } catch (error) {
    log.warn(`Joi: ${error.message}`);
    return res.status(400).send(`${error.name}: ${error.details[0].message} `);
  }

  try {
    const rows = await db.query(`CALL create_club('${req.body.name}', '${req.body.description}', ${req.player.id});`);
    if (!rows[0][0][0]) {
      log.warn('logger="postClub" err="response from db is empty"');
      return res.sendStatus(500);
    } else {
      return res.status(201).send(rows[0][0][0]);
    }
  } catch (error) {
    if (error.errno === 1062) {
      res.status(409).send(error);
    } else {
      res.status(500).send(error);
    }
    log.error(error.message);
    return res.sendStatus(500).send(error);
  }

});

module.exports = clubs;