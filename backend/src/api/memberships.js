const express = require('express');
const memberships = express.Router();
const addPlayerInfoToReq = require('../middleware/addPlayerInfoToReq');
const Joi = require('joi');
const { createMembership, } = require('../schemas/memberships');
const log = require('../common/logger');
const db = require('../db/db_connection');

memberships.post('/', addPlayerInfoToReq, (req, res) => {
  Joi.validate(req.body, createMembership)
    .then(() => {
      db.query(`CALL createMembership(${req.body.player}, ${req.body.club}, ${req.body.role})`)
        .then(() => {
          res.sendStatus(201);
        })
        .catch(err => {
          res.status(500).send(err);
          log.error(err.message);
        });
    })
    .catch(err => {
      res.status(400).send(err);
    });
});

module.exports = memberships;

// Update a Membership
//  - set a role for a player in a club

// Delete Membership
//  - you should not delete the membership, you should set it to inactive