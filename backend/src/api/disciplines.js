const express = require('express');
const disciplines = express.Router();
const Joi = require('joi');
const db = require('../db/db_connection');
const log = require('../common/logger');
const { createDiscipline, updateDiscipline, deleteDiscipline, getDisciplines, } = require('../schemas/disciplines');
const addPlayerInfoToReq = require('../middleware/addPlayerInfoToReq');
const addMemRole = require('../middleware/addMemRole');

disciplines.post('/', addPlayerInfoToReq, addMemRole, async (req, res) => {
  if (req.memRole === 1 || req.memRole === 2) {
    try {
      await Joi.validate(req.body, createDiscipline);
    } catch (error) {
      log.warn(`${error.message}`);
      return res.status(400).send(error);
    }
    try {
      let callProc = '';
      if (!req.body.description) {
        callProc = `CALL createDisciplineNoDesc('${req.body.name}', ${req.body.club});`;
      } else {
        callProc = `CALL createDiscipline('${req.body.name}', '${req.body.description}', '${req.body.club}');`;
      }
      const rows = await db.query(callProc);
      if (!rows[0][0][0]) {
        log.warn('logger="postDiscpline" err="response from db is empty"');
        return res.sendStatus(500);
      } else {
        return res.status(201).send(rows[0][0][0]);
      }
    } catch (error) {
      log.error(`${error.message}`);
      return res.status(500).send(error);
    }
  } else {
    res.sendStatus(403);
  }
});

disciplines.put('/', addPlayerInfoToReq, addMemRole, async (req, res) => {
  if (req.memRole === 1 || req.memRole === 2) {
    try {
      await Joi.validate(req.body, updateDiscipline);
    } catch (error) {
      log.error(`${error.message}`);
      return res.status(400).send(error);
    }
    try {
      let callProc = '';
      if (!req.body.description) {
        callProc = `CALL updateDisciplineNoDesc(${req.body.id}, '${req.body.name}');`;
      } else {
        callProc = `CALL updateDiscipline('${req.body.id}', '${req.body.name}', '${req.body.description}');`;
      }
      const rows = await db.query(callProc);
      const updatedDiscipline = rows[0][0][0];
      if (!updatedDiscipline) {
        return res.sendStatus(404);
      } else {
        return res.send(updatedDiscipline);
      }
    } catch (error) {
      log.error(`${error.message}`);
      return res.status(500).send(error);
    }
  }
});

disciplines.delete('/', addPlayerInfoToReq, addMemRole, async (req, res) => {
  if (req.memRole === 1 || req.memRole === 2) {
    try {
      await Joi.validate(req.body, deleteDiscipline);
    } catch (error) {
      log.error(`${error.message}`);
      return res.status(400).send(error);
    }
    try {
      await db.query(`CALL deleteDiscipline(${req.body.id});`);
      return res.sendStatus(200);
    } catch (error) {
      log.error(`${error.message}`);
      return res.status(400).send(error);
    }
  } else {
    return res.sendStatus(403);
  }
});

disciplines.get('/', addPlayerInfoToReq, async (req, res) => {
  try {
    await Joi.validate(req.body, getDisciplines);
  } catch (error) {
    log.error(`${error.message}`);
    return res.status(400).send(error);
  }

  try {
    let rows = await db.query(`CALL getDisciplinesFromClub('${req.body.club}');`);
    if (!rows[0][0][0]) {
      return res.sendStatus(204);
    } else {
      const disciplines = rows[0][0];
      return res.status(200).send(disciplines);
    }
  } catch (error) {
    log.error(`${error.message}`);
    return res.status(500).send(error);
  }
});

module.exports = disciplines;
