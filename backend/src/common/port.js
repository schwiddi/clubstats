const fallbackPortHttp = '8080';
const httpPort = process.env.BACKEND_HTTP_PORT || fallbackPortHttp;

module.exports = {
  httpPort: httpPort,
};