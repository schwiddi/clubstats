const Joi = require('joi');

const schemas = {
  createMembership: {
    player: Joi.number()
      .min(1)
      .greater(0)
      .integer()
      .positive()
      .required(),
    club: Joi.number()
      .min(1)
      .greater(0)
      .integer()
      .positive()
      .required(),
    role: Joi.number()
      .min(1)
      .greater(0)
      .integer()
      .positive()
      .required(),
  },
};

module.exports = {
  createMembership: schemas.createMembership,
};