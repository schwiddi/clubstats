const Joi = require('joi');

const schemas = {
  invitePlayer: {
    player: Joi.number()
      .min(1)
      .greater(0)
      .integer()
      .positive()
      .required(),
    email: Joi.string()
      .email({ minDomainAtoms: 2, })
      .min(5)
      .max(191)
      .required(),
  },
};

module.exports = {
  invitePlayer: schemas.invitePlayer,
};