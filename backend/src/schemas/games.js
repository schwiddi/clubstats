const Joi = require('joi');

const schemas = {
  createGame: {
    club: Joi.number()
      .min(1)
      .greater(0)
      .integer()
      .positive()
      .required(),
    discipline: Joi.number()
      .min(1)
      .greater(0)
      .integer()
      .positive()
      .required(),
    participants: Joi.array()
      .length(2)
      .unique()
      .items(
        Joi.number()
          .min(1)
          .greater(0)
          .integer()
          .positive()
          .required())
      .required(),
    winner: Joi.number()
      .min(1)
      .greater(0)
      .integer()
      .positive()
      .required(),
  },
  deleteGames: {
    club: Joi.number()
      .min(1)
      .greater(0)
      .integer()
      .positive()
      .required(),
    games: Joi.array()
      .min(1)
      .items(Joi.number())
      .required(),
  },
};

module.exports = {
  createGame: schemas.createGame,
  deleteGames: schemas.deleteGames,
};