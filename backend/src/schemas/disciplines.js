const Joi = require('joi');

const schemas = {
  createDiscipline: {
    name: Joi.string()
      .min(3)
      .max(191)
      .required(),
    description: Joi.string()
      .optional()
      .max(1000),
    club: Joi.number()
      .min(1)
      .greater(0)
      .integer()
      .positive()
      .required(),
  },
  updateDiscipline: {
    id: Joi.number()
      .min(1)
      .greater(0)
      .integer()
      .positive()
      .required(),
    name: Joi.string()
      .min(3)
      .max(191)
      .required(),
    description: Joi.string()
      .optional()
      .max(1000),
    club: Joi.number()
      .min(1)
      .greater(0)
      .integer()
      .positive()
      .required(),
  },
  deleteDiscipline: {
    id: Joi.number()
      .min(1)
      .greater(0)
      .integer()
      .positive()
      .required(),
    club: Joi.number()
      .min(1)
      .greater(0)
      .integer()
      .positive()
      .required(),
  },
  getDisciplines: {
    club: Joi.number()
      .min(1)
      .greater(0)
      .integer()
      .positive()
      .required(),
  },
};

module.exports = {
  createDiscipline: schemas.createDiscipline,
  updateDiscipline: schemas.updateDiscipline,
  deleteDiscipline: schemas.deleteDiscipline,
  getDisciplines: schemas.getDisciplines,
};
