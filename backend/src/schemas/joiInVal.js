const Joi = require('joi');

const schemas = {
  registerPlayer: {
    name: Joi.string()
      .min(1)
      .max(255)
      .required(),
    email: Joi.string()
      .email({ minDomainAtoms: 2, })
      .min(5)
      .max(255)
      .required(),
    password: Joi.string()
      .min(5)
      .max(255)
      .required(),
  },
  do_auth: {
    email: Joi.string()
      .email({ minDomainAtoms: 2, })
      .min(5)
      .max(255)
      .required(),
    password: Joi.string()
      .min(5)
      .max(255)
      .required(),
  },
  create_club: {
    name: Joi.string()
      .min(1)
      .max(255)
      .required(),
    description: Joi.string()
      .max(1000),
  },
  createSimplePlayer: {
    name: Joi.string()
      .min(1)
      .max(191)
      .required(),
    club: Joi.number()
      .min(1)
      .greater(0)
      .integer()
      .positive()
      .required(),
  },
};

module.exports = {
  registerPlayer: schemas.registerPlayer,
  do_auth: schemas.do_auth,
  create_club: schemas.create_club,
  createSimplePlayer: schemas.createSimplePlayer,
};
