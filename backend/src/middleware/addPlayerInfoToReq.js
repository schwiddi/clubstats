const log = require('../common/logger');
const jwt = require('jwt-then');
const db = require('../db/db_connection');

module.exports = function addPlayerInfoToReq(req, res, next) {
  req.player = {};
  const token = req.header('x-auth-token');
  if (!token) {
    res.sendStatus(401);
    return;
  } else {
    req.player.token = token;
  }

  jwt.verify(req.player.token, process.env.BACKEND_JWT_KEY)
    .then(payload => {
      req.player.id = payload.player_id;
      db.query(`CALL get_player_state_by_player_id('${req.player.id}');`)
        .then(rows => {
          const playerState = rows[0][0][0].state;
          if (!playerState) {
            log.error('logger="addPlayerInfoToReq" msg="should never occure"');
            res.sendStatus(500);
            return;
          } else {
            req.player.state = playerState;
            next();
          }
        })
        .catch(err => {
          log.error(err.message);
          res.sendStatus(500);
          return;
        });
    })
    .catch(err => {
      log.error(`logger="addPlayerInfoToReq" err="${err.message}"`);
      res.sendStatus(401);
      return;
    });
};
