const db = require('../db/db_connection');
const log = require('../common/logger');

module.exports = function addMemRole(req, res, next) {
  db.query(`CALL getMemRole(${req.player.id}, ${req.body.club});`)
    .then(rows => {
      if (!rows[0][0][0]) {
        return res.sendStatus(403);
      } else {
        req.memRole = rows[0][0][0].role;
        next();
      }
    })
    .catch(err => {
      log.error(err.message);
      res.status(500).send(err);
    });
};
