describe('port stuff', () => {
  const oldEnv = process.env;

  beforeEach(() => {
    jest.resetModules();
    process.env = { ...oldEnv, };
    delete process.env.BACKEND_HTTP_PORT;
  });

  afterEach(() => {
    process.env = oldEnv;
  });

  test('should return fallback without env', () => {
    const testedModule = require('../common/port');
    expect(testedModule.httpPort).toBeDefined();
    expect(testedModule.httpPort).toBe('8080');
  });

  test('should return ports from env', () => {
    process.env.BACKEND_HTTP_PORT = '6666';
    const testedModule = require('../common/port');
    expect(testedModule.httpPort).toBeDefined();
    expect(testedModule.httpPort).toBe('6666');
  });
});