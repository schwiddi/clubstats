describe('Joi Input Validation Schmema Tests', () => {
  test('new player schema should be present', () => {
    const { registerPlayer, } = require('../schemas/joiInVal');
    expect(registerPlayer).toBeDefined();
  });
  test('auth schema should be present', () => {
    const { do_auth, } = require('../schemas/joiInVal');
    expect(do_auth).toBeDefined();
  });
  test('club schema should be present', () => {
    const { create_club, } = require('../schemas/joiInVal');
    expect(create_club).toBeDefined();
  });
  // test('disciplin schema should be present', () => {
  //   const { create_discipline, } = require('../schemas/joiInVal');
  //   expect(create_discipline).toBeDefined();
  // });
});