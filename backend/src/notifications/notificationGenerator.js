const db = require('../db/db_connection');
const log = require('../common/logger');

function notificationGenerator() {
  setInterval(() => {
    let players, types;
    db.query(`
      SELECT
        p.id,
        p.name,
        p.email,
        p.state AS stateid,
        ps.name AS statename,
        p.role,
        p.registration_key,
        p.inserted
      FROM players p
        JOIN (player_states ps) ON (p.state = ps.id)
      WHERE ps.name = 'registered';
      `)
      .then(rows => {
        players = rows[0];
        return db.query('SELECT * FROM mailing_types;');
      })
      .then((rows) => {
        types = rows[0];
        players.forEach(function (player) {
          types.forEach(function (type) {
            db.query(`SELECT * FROM mailings WHERE player = ${player.id} AND type = ${type.id};`)
              .then(rows => {
                if (!rows[0][0]) {
                  db.query(`INSERT INTO mailings (player, type) VALUES ('${player.id}', '${type.id}');`)
                    .then(() => {
                      log.info(`notificationGenerator: Inserted notification="${type.id}" player="${player.id}"`);
                    });
                }
              });
          });
        });
      })
      .catch(err => {
        log.error(err.message);
      });
  }, 5000);
}

module.exports = notificationGenerator;